<div>
    @extends('layouts.plantilla')

    @section('content')
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Empleados</div>
                        <div class="card-body">
                            <table class="table table-bordered table-striped employees_table" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">nombre</th>
                                        <th scope="col">descripcion</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @push('js')

        <script>
            $(document).ready(function() {
                $('.employees_table').DataTable({
                    processing: true,
                    serverSide: true,
                    responsive: true,
                    ajax: "{{ route('cuentas.listar') }}",
                    dataType: 'json',
                    type: "POST", 
                    columns: [{
                            data: 'id',
                            name: 'id'
                        },
                        {
                            data: 'nombre',
                            name: 'nombre',
                        },
                        {
                            data: 'descripcion',
                            name: 'descripcion'
                        },
                        {
                            data: 'actions',
                            name: 'actions',
                            searchable: false,
                            orderable: false
                        }
                    ],
                })
            })
        </script>
    @endpush
</div>
