<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Models\Cuenta;



class CuentasController extends Controller
{

    public function index(){
        return "hola";
    }
    public function listar(Request $request)
    {
        if ($request->ajax()) {

            $cuentas = Cuenta::select('id','nombre','descripcion');

            return DataTables::of($cuentas)
                ->addColumn('actions', 'actions') //nombre columna, vista
                ->editColumn('actions', function($cuentas) {
                    return view('acciones', compact('cuentas'));
                })->make(true); //procesa la vista html de la columna actions;
               
        }


        return view('cuentas');
    }
    public function edit($id){
        return $id;
    }
    public function destroy($id){
        
    }
    public function create(){
        return view('create');
    }
}
