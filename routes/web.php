<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CuentasController;
use Illuminate\Support\Facades\Auth;

Route::get('/', function () {
    return view('welcome');
});

Route::get('cuentas/listar', [CuentasController::class, 'listar'])->name('cuentas.listar');
Route::resource('cuentas', CuentasController::class);

Auth::routes();

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
