<div>
    @extends('layouts.plantilla')

    @section('contenido')
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header">Empleados</div>
                        <div class="card-body">
                            <div class="table-responsive">
                            <table class="table table-bordered table-hover employees_table"  style="width: 100%;">
                                <thead class="thead-dark table-hover">
                                    <tr>
                                        <th scope="col">id</th>
                                        <th scope="col">nombre</th>
                                        <th scope="col">descripcion</th>
                                        <th scope="col">Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="paila" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        ...
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </div>
    @endsection

    @push('js')

        <script>
            $.fn.dataTable.ext.buttons.alert = {
                className: 'buttons-alert',

                // action: function(e, dt, node, config) {
                //     alert(this.text());
                // }

                action: function(e, dt, node, config) {
                    '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#paila">Launch demo modal</button>'
                    // "<a href='{{ route('cuentas.create') }}' class='btn btn-success'>Edit</a>";
                }

            };
            $(document).ready(function() {


                $('.employees_table').DataTable({
                    processing: true, //sirve para mostrar mensaje cuando son muchos registros
                    serverSide: true, //procesamiento del lado del servidor
                    "lengthMenu": [10, 25, 50, 100], //opciones de tamaño de filas por pagina
                    fixedHeader: true, //habilitar encabezado fijo
                    "pageLength": 5, //tamaño inicial de filas por pagina
                    responsive: true,
                    select: true,
                    // sPaginationType: "bootstrap-4",
                    "pagingType": "full_numbers",   //paginacion: 'Primero', 'Anterior', 'Siguiente' y 'Último', más números de página
                    ajax: "{{ route('cuentas.listar') }}",
                    // ajax: "/cuentas/listar",
                    dataType: 'json',
                    type: "POST",
                    "sPaginationType": "full",
                    language: {
                        // "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                        "url": "{{ asset('datatable.json') }}"
                        // "url": "/datatable.json"
                    },
                    dom: 'Bfrtip',
                    buttons: [{
                            extend: 'copy',
                            className: 'btn-light',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        'pageLength',
                        {
                            extend: 'print',
                            className: 'fas fa-print fa-lg',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdf',
                            className: 'btn-danger',
                            download: 'open',
                            title: 'Cuentas',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excel',
                            className: 'btn-success',
                            title: 'Cuentas',
                            messageTop: 'The information in this table is copyright to Sirius Cybernetics Corp.',
                            autoFilter: true,
                            sheetName: 'Cuentas',
                            titleAttr: 'Exportar en excel',
                            exportOptions: {       
                                // columns: ':visible' //esportar registros solo de las columnas visibles
                                columns: [ 0, 1, 2]  // se exportar solo las columnas especificas

                            }


                        },
                        {
                            extend: 'alert',
                            text: 'Nuevo'
                        },
                        {
                            extend: 'colvis',
                            className: 'btn-light',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },

                    ],
                    columns: [{
                            data: 'id',
                            name: 'id'
                        },
                        {
                            data: 'nombre',
                            name: 'nombre',
                        },
                        {
                            data: 'descripcion',
                            name: 'descripcion'
                        },
                        {
                            data: 'actions',
                            name: 'actions',
                            searchable: false,
                            orderable: false
                        }
                    ],


                });
            });
        </script>
    @endpush
</div>
